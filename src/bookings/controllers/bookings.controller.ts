import { Body, Controller, Get, Post } from '@nestjs/common';
import { BookingResponseDto, ConsultAvailabilityDto, CreateBookingDto } from '../dto';
import { BookingsService } from '../services/bookings.service';

@Controller('api/bookings')
export class BookingsController {
    constructor(private bookingService: BookingsService){}

    @Get()
    getAll(): Promise<BookingResponseDto[]> {
        return  this.bookingService.findAll();
    }

    @Post()
    saveBooking(@Body() body: CreateBookingDto ) : Promise<BookingResponseDto[]> {
        return this.bookingService.create(body);
    }

    @Post('availables')
    async consultAvailability(@Body() body: ConsultAvailabilityDto ) {
        const booking = await this.bookingService.consultAvailability(body);
        return booking;
    }
}
