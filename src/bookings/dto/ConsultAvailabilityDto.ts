import { IsISO8601, IsNotEmpty, IsNumber } from "class-validator";
import { messageProperty } from "src/utils/messageProperty";

export class ConsultAvailabilityDto {

    @IsNotEmpty({
        message: messageProperty('es requerida'),
    })
    @IsNumber({} ,{
        message: messageProperty('debe ser un número.'),
    })
    numberPerson: number;

    @IsNotEmpty({
        message: messageProperty('es requerida'),
    })
    @IsISO8601({}, {
        message: messageProperty('debe ser un texto con formato YYYY-MM-DD')
    })
    date: string;
}