export class BookingResponseDto {
    id: number;
    name: string;
    telephone: string;
    hour: number;
    numberPerson: number;
    date: string;
}