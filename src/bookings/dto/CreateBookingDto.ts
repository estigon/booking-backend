import { IsNotEmpty, IsNumber, IsISO8601, Matches, MinDate } from 'class-validator';
import { messageProperty } from 'src/utils/messageProperty';

export class CreateBookingDto {
   
    @IsNotEmpty({
        message: messageProperty('es requerida'),
    })
    name: string;
  
    @IsNotEmpty({
        message: messageProperty('es requerida'),
    })
    @Matches(/^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]{8,14}$/, {
        message: messageProperty('debe ser un número de teléfono válido.'),
    })
    telephone: string;
  
    @IsNotEmpty({
        message: messageProperty('es requerida'),
    })
    @IsNumber({} ,{
        message: messageProperty('debe ser un número.'),
    })
    hour: number;
  
    @IsNotEmpty({
        message: messageProperty('es requerida'),
    })
    @IsNumber({} ,{
        message: messageProperty('debe ser un número.'),
    })
    numberPerson: number;
  
    @IsNotEmpty({
        message: messageProperty('es requerida'),
    })
    @IsISO8601({}, {
        message: messageProperty('debe ser un texto con formato YYYY-MM-DD')
    })
    date: string;
}
