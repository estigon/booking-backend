export class ConsultAvailabilityResponseDto {
    isAvailable: boolean;
    message: string;
    availability: number;
}