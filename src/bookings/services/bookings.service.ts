import { HttpException, HttpStatus, Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToInstance } from 'class-transformer';
import { dateGreaterThanOrEqualToday } from 'src/utils/validateDate';
import { Repository } from 'typeorm';
import { BookingResponseDto, ConsultAvailabilityResponseDto, ConsultAvailabilityDto, CreateBookingDto } from '../dto';
import { Booking } from '../entities/booking.entity';

@Injectable()
export class BookingsService {
    constructor(
        @InjectRepository(Booking) private bookingRepository : Repository<Booking>
    ){}

    async findAll() : Promise<BookingResponseDto[]> {
        const booking = await this.bookingRepository.find();
        return plainToInstance(BookingResponseDto, booking);
    }

    async findOne(id: number) : Promise<BookingResponseDto> {
        const booking = await this.bookingRepository.findOne({where: {
            id
        }});

        if(!booking) {
            throw new NotFoundException(`No se encuentra la reserva con id ${id}.`)
        }

        return plainToInstance(BookingResponseDto, booking);
    }

    async create(body: CreateBookingDto) : Promise<BookingResponseDto[]> {
        const { numberPerson, date } = body;

        // validate date greater than or equal to today
        if(!dateGreaterThanOrEqualToday(date)){
            throw new HttpException('La fecha debe ser igual o posterior a hoy.', HttpStatus.BAD_REQUEST);
        }

        const availability = await this.consultAvailability({
            date, numberPerson
        });

        if(!availability.isAvailable){
            throw new HttpException('No es posible reservar para este día. No hay espacio disponible.', HttpStatus.BAD_REQUEST);
        }
        
        if( numberPerson <=  availability.availability) {
            const newBooking = this.bookingRepository.create(body as any);
            const booking = await this.bookingRepository.save(newBooking);
            return plainToInstance(BookingResponseDto, booking);
        }
        
        throw new HttpException(`Solo queda espacio para ${availability.availability} personas.`, HttpStatus.BAD_REQUEST)
    }

    async update(id: number, body: any) {
        const booking = await this.bookingRepository.findOne({ where: { id }});
        if(booking){
            this.bookingRepository.merge(booking, body);
            return this.bookingRepository.save(booking);
        }
        throw new NotFoundException(`No se encuentra la reserva con id ${id}.`)
    }

    async delete(id: number) {
        const booking =  await this.bookingRepository.delete(id);
        return true;
    }

    async consultAvailability(body: ConsultAvailabilityDto) : Promise<ConsultAvailabilityResponseDto> {
        const MAX_PEOPLE_PER_DAY = 20;
        
        const bookings = await this.bookingRepository.find({
            where: {
                date: body.date as any
            }
        });

        let personNumberWithBooking = 0;

        if(bookings.length > 0) {
            bookings.forEach(booking => {
                personNumberWithBooking += booking.numberPerson;
            });
        }

        if(personNumberWithBooking < MAX_PEOPLE_PER_DAY){
            const emptyPlaces =  (MAX_PEOPLE_PER_DAY - personNumberWithBooking);
            return {
                isAvailable: true,
                availability: emptyPlaces,
                message: `Existe disponibilidad para este día, para ${emptyPlaces} personas`
            } as ConsultAvailabilityResponseDto;
        }
        return {
            isAvailable: false,
            availability: 0,
            message: `No existe disponibilidad para este día.`
        } as ConsultAvailabilityResponseDto;
    }
}
