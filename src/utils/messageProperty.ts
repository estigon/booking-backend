export function messageProperty(message: string): string {
    return `La propiedad $property ${message}`;
}