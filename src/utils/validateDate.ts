export function dateGreaterThanOrEqualToday(date: string) : boolean {
    const date1 = new Date(date);
    const date2 = new Date();
    const today = new Date(date2.toISOString().slice(0,10));
    
    return date1 >= today
}